# Change log

## 2.1.3

### Bug fixes

* find: play nicely with TSESTree ASTs (1bfe752d6548e1b626897eb6c08f9d24620912cc)

### Refactorings

* find: cleaner iteration logic (3f92141c32c235622caeff0aa9791ada58d46f98)

### Other changes

* find: ditch generic typedefs (260d938f2ba077c67579b0e36c046b66d86dea7a)
* package: stop specifying auto-included files (fe691cf7bd096f65d47b583ac50e0f5c58773ce1)

## 2.1.2

### Bug fixes

* find: sanely handle symbol keys (84eab4bed5c3cde0a2f6dc4ebd95e56e6bcdd0aa)

### Other changes

* package: drop some files from the install bundle (8a3ad29ee34384a5acb881b1cfb241132e175750)

## 2.1.1

### Refactorings

* find: make types generic (2100245f6909c651c7acc810c9470c9d77efc750)

## 2.1.0

### New features

* find: implement `entering` and `exiting` visitor phases (105b430975e8049d02f21bb7019d553133fef532)

## 2.0.1

### Bug fixes

* types: export `VisitorContext` type (251b13961cf42239accde3d86f4c8eb9a73a99d3)

### Refactorings

* tests: swap out mocha for node builtin test runner (943225180166020738a3e91b19304499edef8880)

### Other changes

* docs: tweak readme (8a2fc009044f6c2fbe4b8e5c59e677649420b294)

## 2.0.0

### Breaking changes

* find: fix visitor pattern but break old single-match behaviour (77fa552d40842029a406f0853c2f0c2497f46733)

### Bug fixes

* find: preserve array keys as integers (7662cb6a9d12bd811ce3b4c3e419e80a7563fe5b)

### Other changes

* docs: tweak readme (64548424ee49514837bbc41285baaccd1aa0b0e9)

## 1.0.0

### Breaking changes

* package: migrate from commonjs to modules (5733eeb8a1837cfef93c0a45f1d2ddc18933ce8c)

### New features

* find: implement breadth-first search as an option (c2b086277829b1f2ea379a100c45543fef510a84)

### Refactorings

* tests: migrate tests to typescript (9819632060341e4c844d00f324d303a8a034622a)
* project: migrate source to typescript (e69962ba71d067417687b195c523c54630a08352)

### Other changes

* ci: `mv eslint.config.mjs eslint.config.js` (a043279774dd20a3804290204c70669c0d08e961)
* ci: add makefile (6e82909ad1177370e348ceb4b7fb224a95f85a26)

## 0.2.0

### New features

* find: implement visitor pattern with predicate `criteria` (6b8418a6fd385cfa2d2928a43116edfac673495a)

### Refactorings

* find: stop abusing try/catch/throw for flow control (651a33286a2fc5817dba141541728614555eb5c8)

### Other changes

* tests: assert node identity (9043657f518d52fe1c485718a4768da35e3f4a4b)

## 0.1.2

### Other changes

* find: prefer spread over concat (f2b74758da6e822616c2e3376ea6f3faa3990220)
* deps: update dev dependencies (797282bcefebfd541a14ac82b23223f520051383)
* find: use `Object.values` for iteration (abd8fb6ae20268b7fba0fe3dc1127a966674f72a)
* ci: run tests in node 18/20/22 (0d762e13cbce2ad6e65a0f1a54f01308f3bb9c14)
* ci: don't test in node 4 (43dedac9a67c42c019bc588c511524386841a76a)
* project: remove travis config (c0ff0bc4d65cf005f0200865d12fd49ed1a0c87e)

## 0.1.1

### Other changes

* project: migrate to gitlab (540f741)
* package: add release script (665eff3)
* docs: fix mixed tabs/spaces in example code (0b46397)
* package: update the search terms (c763c51)
* docs: tidy up the example code (a5bac13)
* ci: add ci config (a1ac5ce)
* docs: tweak the readme and package description (2593d52)

## 0.1.0

* feature: implement basic node finding (8aadd32)
* fix: sanely handle recursive tree structures (25a90ff)
* feature: add a readme file (4c31789)
* feature: add an option for returning only the first match (695a39a)
* chore: add an explicit test of array:false,recursion:true (0a0be45)
* fix: stop walking if we have an early result (cd2cec3)

## 0.0.0

feat: add project boilerplate

