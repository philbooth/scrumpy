.PHONY: build clean deps lint release t test

build: deps clean
	npm run build

clean:
	npm run clean

deps:
	npm i

lint: build
	npm run lint

release: build
	npm run release

t: test

test: build
	npm t
