# scrumpy

[![Build status](https://gitlab.com/philbooth/scrumpy/badges/master/pipeline.svg)](https://gitlab.com/philbooth/scrumpy/pipelines)
[![Package status](https://img.shields.io/npm/v/scrumpy.svg)](https://www.npmjs.com/package/scrumpy)
[![Downloads](https://img.shields.io/npm/dm/scrumpy.svg)](https://www.npmjs.com/package/scrumpy)
[![License](https://img.shields.io/npm/l/scrumpy.svg)](https://opensource.org/licenses/MIT)

Scrumps the juiciest ~~berries~~ nodes from your trees!

* [You what?](#you-what)
* [What's it useful for?](#whats-it-useful-for)
* [How do I install it?](#how-do-i-install-it)
* [How do I use it?](#how-do-i-use-it)
* [Can I see some examples?](#can-i-see-some-examples)
* [Does it handle recursive/circular tree structures?](#does-it-handle-recursivecircular-tree-structures)
* [Are there types?](#are-there-types)
* [Is there a change log?](#is-there-a-change-log)
* [How do I set up the dev environment?](#how-do-i-set-up-the-dev-environment)
* [What license is it released under?](#what-license-is-it-released-under)

## You what?

Scrumpy traverses arbitrary tree structures,
visiting every node
and either invoking a callback function
or returning subtrees that match some criteria.

## What's it useful for?

One use is discoverng nodes
in abstract syntax trees.

For instance,
if you want to find nodes
representing particular tokens
in the [Mozilla-format AST](https://github.com/estree/estree),
you have to walk the tree
and interrogate every node.

Instead of writing that code manually,
scrumpy does it for you.

## How do I install it?

Via `npm`:

```
npm i scrumpy --save
```

Or if you just want the git repo:

```
git clone https://gitlab.com/philbooth/scrumpy.git
```

## How do I use it?

The package exports one function,
called `scrump`:

```js
import { scrump, VisitorPhase } from 'scrumpy';
```

To visit every node and invoke your own callback,
pass it the source tree and a visitor function:

```js
const results = scrump(tree, visitor);

function visitor(context) {
  // context.key: string or number, the key for the node
  // context.value: the node
  // context.depth: number, indicates how far down the tree structure the node is
  // context.path: array of keys for all ancestors from the root down to the current node
  // context.parent: the parent node
  // context.phase: `VisitorPhase.entering` or `VisitorPhase.exiting`
  // context.root: the root node
  // optionally return `true` to push the current node into results array
}
```

Alternatively,
you can tell it to return all nodes
that match a criteria subtree:

```js
const criteria = {
  foo: 'foo',
  bar: {
    baz: 'baz',
    qux: 'qux',
  },
};

const results = scrump(tree, criteria);
```

With both approaches,
you can also pass a third `options` argument,
which controls the search algorithm:

```js
const results = scrump(tree, visitor, {
  recursive: false,  // Set to false to only search the root level for matches
  array: false,      // Set to false to ignore array items when searching
  all: false,        // Set to false to only return the first match
  depthFirst: false, // Set to false to search breadth-first instead of depth-first
  phases: new Set([
    VisitorPhase.entering, // Set to invoke visitor when nodes are entered
    VisitorPhase.exiting,  // Set to invoke visitor when nodes are exited
  ]),
});
```

All boolean options default to `true` if not set.
Default for the `phases` option is
`new Set([ VisitorPhase.entering ])`.

## Can I see some examples?

Find import statements
in an abstract syntax tree,
using a predicate `criteria`:

```js
const results = scrump(ast, ({ value }) => {
  return value.type === 'ImportDefaultSpecifier' ||
    value.type === 'ImportNamespaceSpecifier' ||
    value.type === 'ImportSpecifier'
})
```

Find returns from a function,
ignoring any nested functions,
using a subtree `criteria`:

```js
const results = scrump(functionNode.body.body, {
  type: 'ReturnStatement'
}, {
  recursive: false,
})
```

Find the first `const` declaration (depth-first):

```js
const [result] = scrump(ast, {
  type: 'VariableDeclaration',
  kind: 'const'
}, {
  all: false,
})
```

Find the first `const` declaration (breadth-first):

```js
const [result] = scrump(ast, {
  type: 'VariableDeclaration',
  kind: 'const'
}, {
  all: false,
  depthFirst: false,
})
```

## Does it handle recursive/circular tree structures?

Yep.

## Are there types?

[Yes](index.d.ts).

```ts
import { scrump, type VisitorContext } from 'scrumpy';

const results = scrump(tree, visitor);

function visitor(context: VisitorContext): boolean {
  // ...
}
```

## Is there a change log?

[Yes](CHANGELOG.md).

## How do I set up the dev environment?

To install the dependencies:

```
npm i
```

To run the tests:

```
npm t
```

To lint the code:

```
npm run lint
```

## What license is it released under?

[MIT](LICENSE).
