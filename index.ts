export function scrump<T> (
  node: unknown,
  criteria: Criteria,
  options: Options = {},
): T[] {
  const {
    all = true,
    array = true,
    depth = 0,
    depthFirst = true,
    key = null,
    parent = null,
    path = [],
    phases = new Set([VisitorPhase.entering]),
    recursive = true,
    root = node,
    skip = false,
    visited = new Set()
  } = options

  const result: T[] = []

  if (!skip) {
    if (visited.has(node)) {
      return result
    }

    visited.add(node)

    if (match(node, criteria, { depth, key, parent, path, phases, root })) {
      result.push(node as T)
    }
  }

  if (Array.isArray(node) && array || isIterable(node) && recursive) {
    result.push(...recur<T>(node, criteria, {
      all,
      array,
      depth,
      depthFirst,
      path,
      phases,
      recursive,
      root,
      visited,
    }))
  }

  if (isVisitor(criteria) && phases.has(VisitorPhase.exiting)) {
    if (
      criteria({
        depth,
        key,
        parent,
        path,
        phase: VisitorPhase.exiting,
        root,
        value: node,
      }) && ! phases.has(VisitorPhase.entering)
    ) {
      result.push(node as T)
    }
  }

  return result
}

function match<T> (
  node: T,
  criteria: Criteria,
  options: MatchOptions,
): boolean {
  if (isVisitor(criteria)) {
    if (options.phases.has(VisitorPhase.entering)) {
      return criteria({
        depth: options.depth,
        key: options.key,
        parent: options.parent,
        path: options.path,
        phase: VisitorPhase.entering,
        root: options.root,
        value: node,
      })
    }

    return false
  }

  if (! isIterable(node)) {
    return node === criteria
  }

  if (! isIterable(criteria)) {
    return false
  }

  return keyIterator(criteria).every((ck, c)  => {
    return keyIterator(node).some((nk, n) => {
      // @ts-expect-error `keyIterator` is typesafe
      return match(n[nk], c[ck], options)
    })
  })
}

function isVisitor (criteria: Criteria): criteria is Visitor {
  return typeof criteria === 'function'
}

function isIterable (node: unknown): node is Iterable {
  return !! node && typeof node === 'object'
}

function keyIterator<T extends Iterable> (node: T): KeyIterator<T> {
  const keys = Array.isArray(node) ?
    Object.keys(node).map(key => parseInt(key)) :
    Reflect.ownKeys(node as object)

  return {
    every (predicate: KeyIteratorPredicate<T>): boolean {
      // @ts-expect-error `keys` is marshalled to the correct type above
      return keys.every(key => predicate(key, node))
    },
    some (predicate: KeyIteratorPredicate<T>): boolean {
      // @ts-expect-error `keys` is marshalled to the correct type above
      return keys.some(key => predicate(key, node))
    },
  }
}

function recur<T> (
  node: Iterable,
  criteria: Criteria,
  options: RecurOptions,
): T[] {
  const results: T[] = []

  keyIterator(node).some((key, iterableNode) => {
    if (! options.all && results.length > 0) {
      return true
    }

    // @ts-expect-error `keyIterator` is typesafe
    results.push(...scrump<T>(iterableNode[key], criteria, {
      ...options,
      array: options.depthFirst ? options.array : false,
      depth: options.depth + 1,
      key,
      parent: iterableNode,
      path: [ ...options.path, key ],
      recursive: options.depthFirst ? options.recursive : false,
    }))

    return false
  })

  if (!options.depthFirst) {
    keyIterator(node).some((key, iterableNode) => {
      if (! options.all && results.length > 0) {
        return true
      }

      // @ts-expect-error `keyIterator` is typesafe
      results.push(...scrump<T>(iterableNode[key], criteria, {
        ...options,
        depth: options.depth + 1,
        key,
        parent: iterableNode,
        path: [ ...options.path, key ],
        skip: true,
      }))

      return false
    })
  }

  return results
}

type Nullable<T> = T | null;

type Criteria = unknown | Visitor;

type Visitor = (context: VisitorContext) => boolean;

export type VisitorContext = {
  depth: number;
  key: Nullable<Key>;
  parent: Nullable<unknown>;
  path: Key[];
  phase: VisitorPhase;
  root: unknown;
  value: unknown;
};

type Key = ArrayKey | ObjectKey;
type ArrayKey = number;
type ObjectKey = string | symbol;

export enum VisitorPhase {
  entering,
  exiting,
};

type Options = PublicOptions & PrivateOptions;

export type PublicOptions = {
  all?: boolean;
  array?: boolean;
  depthFirst?: boolean;
  phases?: Set<VisitorPhase>;
  recursive?: boolean;
};

type PrivateOptions = {
  depth?: number;
  key?: Nullable<Key>;
  parent?: Nullable<unknown>;
  path?: Key[];
  root?: unknown;
  skip?: boolean;
  visited?: Set<unknown>;
};

type MatchOptions = {
  depth: number;
  key: Nullable<Key>;
  parent: Nullable<unknown>;
  path: Key[];
  phases: Set<VisitorPhase>;
  root: unknown;
};

type Iterable = IterableArray | IterableObject;

type IterableArray = { [key: ArrayKey]: unknown }

type IterableObject = { [key: ObjectKey]: unknown };

type KeyIterator<T extends Iterable> = {
  every: KeyIteratorMethod<T>;
  some: KeyIteratorMethod<T>;
};

type KeyIteratorMethod<T extends Iterable> = (predicate: KeyIteratorPredicate<T>) => boolean;

type KeyIteratorPredicate<T extends Iterable> = (key: keyof T, iterable: T) => boolean;

type RecurOptions = {
  all: boolean;
  array: boolean;
  depth: number;
  depthFirst: boolean;
  path: Key[];
  phases: Set<VisitorPhase>;
  recursive: boolean;
  root: unknown;
  visited: Set<unknown>;
};
