import { test } from 'node:test';

import { assert } from 'chai'
import { parse } from '@typescript-eslint/typescript-estree';
import * as sinon from 'sinon'

import { scrump } from './index.js'
import { VisitorPhase } from './index.js'

import type { TSESTree } from '@typescript-eslint/typescript-estree';
import type { VisitorContext } from './index.ts'

test('interface is correct', () => {
  assert.isFunction(scrump)
  // @ts-expect-error `scrump` is a function, functions have a `length` property
  assert.lengthOf(scrump, 2)
})

test('finds match at root', () =>
  assert.deepEqual(scrump({ foo: 'bar' }, { foo: 'bar' }), [ { foo: 'bar' } ])
)

test('finds match among many properties', () =>
  assert.deepEqual(
    scrump(
      { foo: 'bar', baz: 'qux', wibble: 'blee' },
      { baz: 'qux' }
    ),
    [ { foo: 'bar', baz: 'qux', wibble: 'blee' } ]
  )
)

test('finds match in descendants', () =>
  assert.deepEqual(
    scrump(
      { foo: { bar: { baz: 'qux' } } },
      { baz: 'qux' }
    ),
    [ { baz: 'qux' } ]
  )
)

test('finds match spanning generations', () =>
  assert.deepEqual(
    scrump(
      { foo: 'foo', bar: { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } } },
      { baz: { qux: 'qux' } }
    ),
    [ { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } } ]
  )
)

test('finds multiple matches', () =>
  assert.deepEqual(
    scrump(
      { foo: 'foo', bar: { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } }, baz: { baz: { wibble: 'wibble', blee: 'blee', qux: 'qux' } } },
      { baz: { qux: 'qux' } }
    ),
    [
      { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } },
      { baz: { wibble: 'wibble', blee: 'blee', qux: 'qux' } }
    ]
  )
)

test('finds every match under a common root', () =>
  assert.deepEqual(
    scrump(
      { foo: 'foo', bar: { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } }, baz: { wibble: 'wibble', blee: 'blee', qux: 'qux' } },
      { baz: { qux: 'qux' } }
    ),
    [
      { foo: 'foo', bar: { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } }, baz: { wibble: 'wibble', blee: 'blee', qux: 'qux' } },
      { bar: 'bar', baz: { baz: 'baz', qux: 'qux' } },
    ]
  )
)

test('does not find match in descendants when recursion is disabled', () =>
  assert.deepEqual(
    scrump(
      { foo: { bar: { baz: 'qux' } } },
      { baz: 'qux' },
      { recursive: false }
    ),
    []
  )
)

test('finds matches in array descendants', () =>
  assert.deepEqual(
    scrump(
      [ { foo: 'bar' }, { baz: 'qux', v: 1 }, { wibble: 'blee', glarg: { baz: 'qux', v: 2 } } ],
      { baz: 'qux' }
    ),
    [ { baz: 'qux', v: 1 }, { baz: 'qux', v: 2 } ]
  )
)

test('finds immediate match in array descendants when recursion is disabled but arrays are enabled', () =>
  assert.deepEqual(
    scrump(
      [ { foo: 'bar' }, { baz: 'qux', v: 1 }, { wibble: 'blee', glarg: { baz: 'qux', v: 2 } } ],
      { baz: 'qux' },
      { recursive: false }
    ),
    [ { baz: 'qux', v: 1 } ]
  )
)

test('finds matches in array descendants when array is disabled but recursion is enabled', () =>
  assert.deepEqual(
    scrump(
      [ { foo: 'bar' }, { baz: 'qux', v: 1 }, { wibble: 'blee', glarg: { baz: 'qux', v: 2 } } ],
      { baz: 'qux' },
      { array: false }
    ),
    [ { baz: 'qux', v: 1 }, { baz: 'qux', v: 2 } ]
  )
)

test('does not find match in array descendants when recursion and arrays are disabled', () =>
  assert.deepEqual(
    scrump(
      [ { foo: 'bar' }, { baz: 'qux' }, { wibble: 'blee', glarg: { baz: 'qux' } } ],
      { baz: 'qux' },
      { recursive: false, array: false }
    ),
    []
  )
)

test('finds matches in non-root array descendants', () =>
  assert.deepEqual(
    scrump(
      { array: [ { foo: 'bar' }, { baz: 'qux', v: 1 }, { wibble: 'blee', glarg: { baz: 'qux', v: 2 } } ] },
      { baz: 'qux' }
    ),
    [ { baz: 'qux', v: 1 }, { baz: 'qux', v: 2 } ]
  )
)

test('finds match in infinitely recursive trees', () => {
  type Recursive = {
    foo: {
      bar: {
        baz: {
          qux: 'qux';
        };
        qux?: Recursive;
      };
    };
  };
  const recursive: Recursive = {
    foo: {
      bar: {
        baz: {
          qux: 'qux'
        },
      }
    }
  }
  recursive.foo.bar.qux = recursive
  assert.deepEqual(scrump(recursive, { qux: 'qux' }), [ { qux: 'qux' } ])
})

test('finds first match (depth-first)', () => {
  const firstMatch = { qux: 'qux' }
  const data = {
    foo: {
      bar: {
        baz: firstMatch,
      },
      baz: { qux: 'qux' },
    },
  }
  const results = scrump(data, { qux: 'qux' }, { all: false })
  assert.lengthOf(results, 1)
  assert.equal(results[0], firstMatch)
})

test('finds first match (breadth-first)', () => {
  const firstMatch = { qux: 'qux' }
  const data = {
    foo: {
      bar: {
        baz: { qux: 'qux' },
      },
      baz: firstMatch,
    },
  }
  const results = scrump(data, { qux: 'qux' }, { all: false, depthFirst: false })
  assert.lengthOf(results, 1)
  assert.equal(results[0], firstMatch)
})

test('returns actual nodes instead of copies', () => {
  const firstMatch = { baz: 'qux' }
  const secondMatch = { baz: 'qux' }
  const results = scrump(
    { foo: firstMatch, bar: secondMatch },
    { baz: 'qux' }
  )
  assert.lengthOf(results, 2)
  assert.equal(results[0], firstMatch)
  assert.equal(results[1], secondMatch)
})

test('implements visitor pattern when criteria is function', () => {
  const data = { foo: { bar: 'bar', baz: 'baz' }, qux: 'qux' }
  const spy = sinon.spy(({ value }) => typeof value === 'string')

  assert.deepEqual(scrump(data, spy), [ 'bar', 'baz', 'qux' ])

  assert.equal(spy.callCount, 5)

  assert.lengthOf(spy.args[0], 1)
  assert.deepEqual(spy.args[0][0], {
    depth: 0,
    key: null,
    parent: null,
    path: [],
    phase: VisitorPhase.entering,
    root: data,
    value: data,
  })

  assert.lengthOf(spy.args[1], 1)
  assert.deepEqual(spy.args[1][0], {
    depth: 1,
    key: 'foo',
    parent: data,
    path: [ 'foo' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo,
  })

  assert.lengthOf(spy.args[2], 1)
  assert.deepEqual(spy.args[2][0], {
    depth: 2,
    key: 'bar',
    parent: data.foo,
    path: [ 'foo', 'bar' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo.bar,
  })

  assert.lengthOf(spy.args[3], 1)
  assert.deepEqual(spy.args[3][0], {
    depth: 2,
    key: 'baz',
    parent: data.foo,
    path: [ 'foo', 'baz' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo.baz,
  })

  assert.lengthOf(spy.args[4], 1)
  assert.deepEqual(spy.args[4][0], {
    depth: 1,
    key: 'qux',
    parent: data,
    path: [ 'qux' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.qux,
  })
})

test('visitor works breadth-first', () => {
  const data = { foo: { bar: 'bar', baz: 'baz' }, qux: 'qux' }
  const spy = sinon.spy((context: VisitorContext) => typeof context.value === 'string')

  assert.deepEqual(scrump(data, spy, { depthFirst: false }), [ 'qux', 'bar', 'baz' ])

  assert.equal(spy.callCount, 5)

  assert.lengthOf(spy.args[0], 1)
  assert.deepEqual(spy.args[0][0], {
    depth: 0,
    key: null,
    parent: null,
    path: [],
    phase: VisitorPhase.entering,
    root: data,
    value: data,
  })

  assert.lengthOf(spy.args[1], 1)
  assert.deepEqual(spy.args[1][0], {
    depth: 1,
    key: 'foo',
    parent: data,
    path: [ 'foo' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo,
  })

  assert.lengthOf(spy.args[2], 1)
  assert.deepEqual(spy.args[2][0], {
    depth: 1,
    key: 'qux',
    parent: data,
    path: [ 'qux' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.qux,
  })

  assert.lengthOf(spy.args[3], 1)
  assert.deepEqual(spy.args[3][0], {
    depth: 2,
    key: 'bar',
    parent: data.foo,
    path: [ 'foo', 'bar' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo.bar,
  })

  assert.lengthOf(spy.args[4], 1)
  assert.deepEqual(spy.args[4][0], {
    depth: 2,
    key: 'baz',
    parent: data.foo,
    path: [ 'foo', 'baz' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo.baz,
  })
})

test('visitor pattern preserves array keys as numbers', () => {
  const data = [ 'foo', 'bar' ]
  const spy = sinon.spy(() => true)

  assert.deepEqual(scrump(data, spy), [ data, data[0], data[1] ])

  assert.equal(spy.callCount, 3)

  // @ts-expect-error i know what i'm doing
  assert.strictEqual(spy.args[1][0].key, 0)
  // @ts-expect-error i know what i'm doing
  assert.strictEqual(spy.args[1][0].path[0], 0)

  // @ts-expect-error i know what i'm doing
  assert.strictEqual(spy.args[2][0].key, 1)
  // @ts-expect-error i know what i'm doing
  assert.strictEqual(spy.args[2][0].path[0], 1)
})

test('visitor is invoked in both entry and exit phases', () => {
  const data = { foo: { bar: 'bar', baz: 'baz' }, qux: 'qux' }
  const spy = sinon.spy(({ value }) => typeof value === 'string')

  assert.deepEqual(
    scrump(
      data,
      spy,
      { phases: new Set([VisitorPhase.entering, VisitorPhase.exiting]) },
    ),
    [ 'bar', 'baz', 'qux' ],
  )

  assert.equal(spy.callCount, 10)

  assert.lengthOf(spy.args[0], 1)
  assert.deepEqual(spy.args[0][0], {
    depth: 0,
    key: null,
    parent: null,
    path: [],
    phase: VisitorPhase.entering,
    root: data,
    value: data,
  })

  assert.lengthOf(spy.args[1], 1)
  assert.deepEqual(spy.args[1][0], {
    depth: 1,
    key: 'foo',
    parent: data,
    path: [ 'foo' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo,
  })

  assert.lengthOf(spy.args[2], 1)
  assert.deepEqual(spy.args[2][0], {
    depth: 2,
    key: 'bar',
    parent: data.foo,
    path: [ 'foo', 'bar' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo.bar,
  })

  assert.lengthOf(spy.args[3], 1)
  assert.deepEqual(spy.args[3][0], {
    depth: 2,
    key: 'bar',
    parent: data.foo,
    path: [ 'foo', 'bar' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.foo.bar,
  })

  assert.lengthOf(spy.args[4], 1)
  assert.deepEqual(spy.args[4][0], {
    depth: 2,
    key: 'baz',
    parent: data.foo,
    path: [ 'foo', 'baz' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.foo.baz,
  })

  assert.lengthOf(spy.args[5], 1)
  assert.deepEqual(spy.args[5][0], {
    depth: 2,
    key: 'baz',
    parent: data.foo,
    path: [ 'foo', 'baz' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.foo.baz,
  })

  assert.lengthOf(spy.args[6], 1)
  assert.deepEqual(spy.args[6][0], {
    depth: 1,
    key: 'foo',
    parent: data,
    path: [ 'foo' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.foo,
  })

  assert.lengthOf(spy.args[7], 1)
  assert.deepEqual(spy.args[7][0], {
    depth: 1,
    key: 'qux',
    parent: data,
    path: [ 'qux' ],
    phase: VisitorPhase.entering,
    root: data,
    value: data.qux,
  })

  assert.lengthOf(spy.args[8], 1)
  assert.deepEqual(spy.args[8][0], {
    depth: 1,
    key: 'qux',
    parent: data,
    path: [ 'qux' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.qux,
  })

  assert.lengthOf(spy.args[9], 1)
  assert.deepEqual(spy.args[9][0], {
    depth: 0,
    key: null,
    parent: null,
    path: [],
    phase: VisitorPhase.exiting,
    root: data,
    value: data,
  })
})

test('visitor honours exit-only matches', () => {
  const data = { foo: { bar: 'bar', baz: 'baz' }, qux: 'qux' }
  const spy = sinon.spy(({ value }) => typeof value === 'string')

  assert.deepEqual(
    scrump(
      data,
      spy,
      { phases: new Set([VisitorPhase.exiting]) },
    ),
    [ 'bar', 'baz', 'qux' ],
  )

  assert.equal(spy.callCount, 5)

  assert.lengthOf(spy.args[0], 1)
  assert.deepEqual(spy.args[0][0], {
    depth: 2,
    key: 'bar',
    parent: data.foo,
    path: [ 'foo', 'bar' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.foo.bar,
  })

  assert.lengthOf(spy.args[1], 1)
  assert.deepEqual(spy.args[1][0], {
    depth: 2,
    key: 'baz',
    parent: data.foo,
    path: [ 'foo', 'baz' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.foo.baz,
  })

  assert.lengthOf(spy.args[2], 1)
  assert.deepEqual(spy.args[2][0], {
    depth: 1,
    key: 'foo',
    parent: data,
    path: [ 'foo' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.foo,
  })

  assert.lengthOf(spy.args[3], 1)
  assert.deepEqual(spy.args[3][0], {
    depth: 1,
    key: 'qux',
    parent: data,
    path: [ 'qux' ],
    phase: VisitorPhase.exiting,
    root: data,
    value: data.qux,
  })

  assert.lengthOf(spy.args[4], 1)
  assert.deepEqual(spy.args[4][0], {
    depth: 0,
    key: null,
    parent: null,
    path: [],
    phase: VisitorPhase.exiting,
    root: data,
    value: data,
  })
})

test('symbol keys are handled sanely', () => {
  const symbol = Symbol('foo')
  const data = { [symbol]: 'foo' }
  const spy = sinon.spy(({ key }) => typeof key === 'symbol')

  assert.deepEqual(scrump(data, spy), [ 'foo' ])

  assert.equal(spy.callCount, 2)

  assert.lengthOf(spy.args[1], 1)
  assert.deepStrictEqual(spy.args[1][0], {
    depth: 1,
    key: symbol,
    parent: data,
    path: [ symbol ],
    phase: VisitorPhase.entering,
    root: data,
    value: data[symbol],
  })
})

test('TSESTree nodes can be walked without errors', () => {
  const source = `import defaultExport from 'module-name'
function fun(arg) {
  const local = defaultExport(arg)
  return local
}`
  const ast = parse(source)
  const spy = sinon.spy(({ value }) => !! value && typeof value.type === 'string')
  const nodes = scrump(ast, spy)
  assert.lengthOf(nodes, 17)
  assert.isObject(nodes[0])
  assert.equal((nodes[0] as TSESTree.Node).type, 'Program')
  assert.equal((nodes[16] as TSESTree.Node).type, 'Identifier')
  assert.equal(spy.callCount, 53)
})
